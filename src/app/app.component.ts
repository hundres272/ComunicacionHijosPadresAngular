import { Component, ViewChild } from '@angular/core';
import { SelectornumericoComponent } from './selectornumerico/selectornumerico.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'comunicacionHijosPadres';
  @ViewChild('selectorl') selectorl: SelectornumericoComponent;
  @ViewChild('selector2') selector2: SelectornumericoComponent;
  fijarSelector1(valor: number) {
    this.selectorl.fijar(valor);
  }
  fijarSelector2(valor: number) {
    this.selector2.fijar(valor);
  }
}
